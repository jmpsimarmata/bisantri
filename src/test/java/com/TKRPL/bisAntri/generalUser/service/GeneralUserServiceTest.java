//package com.TKRPL.bisAntri.generalUser.service;
//
//import com.TKRPL.bisAntri.model.GeneralUser;
//import com.TKRPL.bisAntri.repository.GeneralUserRepository;
//import com.TKRPL.bisAntri.service.GeneralUserServiceImpl;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestInstance;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.*;
//
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
//@ExtendWith(MockitoExtension.class)
//public class GeneralUserServiceTest {
//
//    @Mock
//    private GeneralUserRepository generalUserRepository;
//
//    @InjectMocks
//    private GeneralUserServiceImpl generalUserService;
//
//    private GeneralUser generalUser;
//
//    private GeneralUser generalUser2;
//
//    @BeforeEach
//    void setUp(){
//        generalUser = new GeneralUser("TestNama",
//                "TestUsername",
//                "TestPassword",
//                "TestEmail",
//                "TestFoto",
//                123,
//                null);
//
//        generalUser2 = new GeneralUser("TestNama2",
//                "TestUsername",
//                "TestPassword2",
//                "TestEmail",
//                "TestFoto2",
//                123,
//                null);
//        generalUser.setIdUser("1");
//        generalUser2.setIdUser("2");
//    }
//
//    @Test
//    void testServiceCreateGeneralUser(){
//        lenient().when(generalUserService.createGeneralUser(generalUser)).thenReturn(generalUser);
//        assertEquals(generalUserService.createGeneralUser(generalUser), generalUser);
//        verify(generalUserRepository).save(generalUser);
//    }
//
//    @Test
//    void testServiceGetGeneralUserByIdUser() {
//        lenient().when(generalUserService.getGeneralUserById(generalUser.getIdUser())).thenReturn(generalUser);
//        assertEquals(generalUser, generalUserService.getGeneralUserById(generalUser.getIdUser()));
//    }
//
//    @Test
//    void testServiceGetGeneralUserByEmail() {
//        lenient().when(generalUserService.getGeneralUserByEmail(generalUser.getEmail())).thenReturn(generalUser);
//        assertEquals(generalUser, generalUserService.getGeneralUserByEmail(generalUser.getEmail()));
//    }
//
//    @Test
//    void testServiceGetGeneralUserByUsername() {
//        lenient().when(generalUserService.getGeneralUserByUsername(generalUser.getUsername())).thenReturn(generalUser);
//        assertEquals(generalUser, generalUserService.getGeneralUserByUsername(generalUser.getUsername()));
//    }
//
//}
