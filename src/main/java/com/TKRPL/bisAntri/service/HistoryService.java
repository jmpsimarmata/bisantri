package com.TKRPL.bisAntri.service;

import com.TKRPL.bisAntri.model.Antrian;
import com.TKRPL.bisAntri.model.History;

import java.util.List;

public interface HistoryService {

    History createHistory(History history);

    List<History> getAllHistory();

    List<History> getAllHistoryByIdGeneralUser(Integer idGeneralUser);

    List<History> getAllHistoryByIdAntrian(Integer idAntrian);

//    History getHistoryByAntrianAndIdGeneralUser(String idAntrian, String idGeneralUser);

}
