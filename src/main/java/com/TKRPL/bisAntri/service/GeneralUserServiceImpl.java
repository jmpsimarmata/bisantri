package com.TKRPL.bisAntri.service;

import com.TKRPL.bisAntri.model.Antrian;
import com.TKRPL.bisAntri.model.GeneralUser;
import com.TKRPL.bisAntri.model.History;
import com.TKRPL.bisAntri.model.Notification;
import com.TKRPL.bisAntri.repository.AntrianRepository;
import com.TKRPL.bisAntri.repository.GeneralUserRepository;
import com.TKRPL.bisAntri.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GeneralUserServiceImpl implements GeneralUserService {

    @Autowired
    private GeneralUserRepository generalUserRepository;
    @Autowired
    private AntrianService AntrianService;

    @Autowired
    private AntrianService antrianService;

    @Autowired
    private AntrianRepository antrianRepository;

    @Autowired
    private HistoryRepository historyRepository;

    @Autowired
    public NotificationService notificationService;

    @Override
    public GeneralUser createGeneralUser(GeneralUser generalUser) {
        //Kalau user dengan email dan username yang di input belum ada di database
//        if(getGeneralUserByEmail(generalUser.getEmail()) == null && getGeneralUserByUsername(generalUser.getUsername()) == null){
//            generalUserRepository.save(generalUser);
//            return generalUser;
//        }
//        return null;
        generalUserRepository.save(generalUser);
        return generalUser;
    }

    @Override
    public GeneralUser getGeneralUserById(Integer idUser) {
        return generalUserRepository.getGeneralUserByIdUser(idUser);
    }

    @Override
    public GeneralUser getGeneralUserByUsername(String username) {
        return generalUserRepository.getGeneralUserByUsername(username);
    }

    @Override
    public GeneralUser getGeneralUserByEmail(String email) {
        return generalUserRepository.getGeneralUserByEmail(email);
    }

    @Override
    public List<GeneralUser> getAllGeneralUsers() {
        return generalUserRepository.findAll();
    }

    @Override
    public GeneralUser loginWithGeneralUsernameAndPassword(String username, String password) {
        GeneralUser user = generalUserRepository.getGeneralUserByUsernameAndPassword(username, password);
        if(user != null){
            return user;
        }
        return null;
    }
    @Override
    public Antrian joinAntrian(Integer idAntrian, Integer idUser) {
        Antrian antrian = antrianService.getAntrianById(idAntrian);

        if (antrian.getKapasitas() == antrian.getJumlahPeserta()) {
            antrian.setStatus(2);
            return null;
        }
        else {
            antrian.setStatus(1);
        }
        antrian.setJumlahPeserta(antrian.getJumlahPeserta() + 1);
        antrianRepository.save(antrian);

        GeneralUser user = getGeneralUserById(idUser);
        user.setCurrentJoinedAntrianId(idAntrian);
        generalUserRepository.save(user);

        History history = new History();
        history.setIdGeneralUser(idUser);
        history.setNomorUrut(antrian.getJumlahPeserta());
        history.setRole("PESERTA");
        history.setIdAntrian(antrian.getIdAntrian());
        history.setNamaAntrian(antrian.getNama());
        history.setStatus(1);
        historyRepository.save(history);

        Notification notification = new Notification(String.format("Halo %s, terima kasih sudah join di %s. Nomor giliran kamu adalah %s dan nomor antrian yang sedang berjalan sekarang adalah %s. Jangan lupa untuk membawa checklist requirements yang sudah diberitahukan di dashboard peserta antrian ya, terima kasih.", user.getNama(), antrian.getNama(), history.getNomorUrut(), antrian.getCurrentNomorAntrian()));
        notificationService.notifyUser(notification, user.getEmail());

        return antrian;
    }
}
