package com.TKRPL.bisAntri.service;

import com.TKRPL.bisAntri.model.Antrian;
import com.TKRPL.bisAntri.model.GeneralUser;

import java.util.List;

public interface GeneralUserService {
    GeneralUser createGeneralUser(GeneralUser generalUser);
    GeneralUser getGeneralUserById(Integer idUser);
    GeneralUser getGeneralUserByUsername(String username);
    GeneralUser getGeneralUserByEmail(String email);
    List<GeneralUser> getAllGeneralUsers();
    GeneralUser loginWithGeneralUsernameAndPassword(String username, String password);
    Antrian joinAntrian(Integer idAntrian, Integer idUser);
}
