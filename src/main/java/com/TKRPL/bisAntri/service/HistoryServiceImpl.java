package com.TKRPL.bisAntri.service;

import com.TKRPL.bisAntri.model.Antrian;
import com.TKRPL.bisAntri.model.History;
import com.TKRPL.bisAntri.repository.HistoryRepository;
import com.TKRPL.bisAntri.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    private HistoryRepository historyRepository;

    //Dipakai ketika suatu peserta join suatu antrian
    //Dipakai ketika suatu admin membuat antrian
    @Override
    public History createHistory(History history) {
        return historyRepository.save(history);
    }

    @Override
    public List<History> getAllHistory() {
        return historyRepository.findAll();
    }

    @Override
    public List<History> getAllHistoryByIdGeneralUser(Integer idGeneralUser) {
        String id = idGeneralUser.toString();
        return historyRepository.getHistoriesByIdGeneralUserOrderByUpdatedTimeDesc(idGeneralUser);
    }

    @Override
    public List<History> getAllHistoryByIdAntrian(Integer idAntrian) {
        return historyRepository.getHistoriesByIdAntrian(idAntrian);
    }


    //Akan dipakai ketika suatu peserta antrian keluar antrian (status history jadi finished)
    //Akan dipakai ketika suatu admin antrian menyelesaikan suatu antrian (status history antrian jadi finished)
//    @Override
//    public History getHistoryByAntrianAndIdGeneralUser(String idAntrian, String idGeneralUser) {
//        return historyRepository.getHistoryByIdAntrianAndiAndIdGeneralUser(idAntrian, idGeneralUser);
//    }
}
