package com.TKRPL.bisAntri.service;

import com.TKRPL.bisAntri.model.Antrian;
import com.TKRPL.bisAntri.model.GeneralUser;
import com.TKRPL.bisAntri.model.History;
import com.TKRPL.bisAntri.model.Notification;
import com.TKRPL.bisAntri.repository.AntrianRepository;
import com.TKRPL.bisAntri.repository.GeneralUserRepository;
import com.TKRPL.bisAntri.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class AntrianService {
    @Autowired
    AntrianRepository antrianRepository;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private GeneralUserRepository generalUserRepository;

    @Autowired
    private HistoryRepository historyRepository;

    @Autowired
    public NotificationService notificationService;

    public List<Antrian> getAllAntrian() {
        return antrianRepository.findAll();
    }

    public Antrian createAntrian(Antrian antrian){
        antrianRepository.save(antrian);
        History historyAntrian = new History();
        historyAntrian.setIdAntrian(antrian.getIdAntrian());
        historyAntrian.setRole("ADMIN");
        historyAntrian.setNamaAntrian(antrian.getNama());
        historyAntrian.setIdGeneralUser(antrian.getPemilikAntrian());
        historyAntrian.setStatus(1);
        historyAntrian.setNomorUrut(null);
        historyService.createHistory(historyAntrian);
        return antrian;
    }

    public Antrian getAntrianById(int id){
        Antrian existingAntrian = antrianRepository.findByIdAntrian(id);
        return existingAntrian;
    }

    public Antrian updateAntrian(Integer id,Antrian newAntrian){
        Antrian existingAntrian = antrianRepository.findByIdAntrian(id);
        if(existingAntrian != null){
            existingAntrian.setNama(newAntrian.getNama());
            existingAntrian.setWaktuBuka(newAntrian.getWaktuBuka());
            existingAntrian.setWaktuTutup(newAntrian.getWaktuTutup());
            existingAntrian.setKapasitas(newAntrian.getKapasitas());
            existingAntrian.setStatus(newAntrian.getStatus());
            existingAntrian.setIdChecklistRequirements(newAntrian.getIdChecklistRequirements());
            existingAntrian.setCurrentNomorAntrian(newAntrian.getCurrentNomorAntrian());
            existingAntrian.setJumlahPeserta(newAntrian.getJumlahPeserta());
            existingAntrian.setPemilikAntrian(newAntrian.getPemilikAntrian());

            antrianRepository.save(existingAntrian);
            return existingAntrian;
        }
        return null;
    }

    public Antrian nextAntrian(int idAntrian){
        Antrian existingAntrian = antrianRepository.findByIdAntrian(idAntrian);
        boolean nomorAntrianUpdated = false;
        if(existingAntrian != null){
            if (existingAntrian.getCurrentNomorAntrian() < existingAntrian.getKapasitas()){
                int noAntrian = existingAntrian.getCurrentNomorAntrian() + 1;
                existingAntrian.setCurrentNomorAntrian(noAntrian);
                nomorAntrianUpdated = true;
            }
        }
        antrianRepository.save(existingAntrian);

        //Kalau berhasil next antrian
        if(nomorAntrianUpdated){
            List<History> listHistory = historyRepository.getHistoriesByIdAntrian(idAntrian);
            for(History history: listHistory){
                if(history.getNomorUrut() == null){
                    continue;
                }
                int selisihNomorUrut = history.getNomorUrut() - existingAntrian.getCurrentNomorAntrian();
                GeneralUser user = generalUserRepository.getGeneralUserByIdUser(history.getIdGeneralUser());
                // Kalau gilirannya sekarang
                if(selisihNomorUrut == 0){
                    Notification notification = new Notification(String.format("Halo %s, sekarang adalah giliran kamu ya di %s. Silahkan untuk bertemu dengan petugas yang berwenang disana. Mohon disertakan checklist requirements yang telah diberitahukan ya, terima kasih.", user.getNama(), existingAntrian.getNama()));
                    notificationService.notifyUser(notification, user.getEmail());
                }else if(selisihNomorUrut >= 1 && selisihNomorUrut <= 3){ //Kalau gilirannya 1-3 lagi
                    Notification notification = new Notification(String.format("Halo %s, kamu sekarang berada di urutan ke-%s sebelum sampai di giliran kamu di %s. Jangan lupa untuk datang dan membawa checklist requirements yang sudah diberitahukan sebelumnya ya, terima kasih",user.getNama(), selisihNomorUrut, existingAntrian.getNama()));
                    notificationService.notifyUser(notification, user.getEmail());
                }
            }
        }
        return existingAntrian;
    }

    public Antrian finishAntrian(int idAntrian){
        Antrian existingAntrian = antrianRepository.findByIdAntrian(idAntrian);
        if(existingAntrian != null){
            existingAntrian.setStatus(0);
        }
        antrianRepository.save(existingAntrian);

        //Kalau antrian finish, history semua orang yang buat atau join jadi inactive statusnya di antrian ini
        List<History> listHistory = historyService.getAllHistoryByIdAntrian(idAntrian);
        for(History history: listHistory){
            history.setStatus(0);
            historyRepository.save(history);
        }

        return existingAntrian;
    }

    public Antrian changeStatusAntrian(int idAntrian){
        Antrian existingAntrian = antrianRepository.findByIdAntrian(idAntrian);
        if(existingAntrian != null){
            if (existingAntrian.getStatus() == 1){
                existingAntrian.setStatus(2);
            }
            else if (existingAntrian.getStatus() == 2){
                existingAntrian.setStatus(1);
            }
        }
        antrianRepository.save(existingAntrian);
        return existingAntrian;
    }
}
