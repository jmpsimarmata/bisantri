package com.TKRPL.bisAntri.service;

import com.TKRPL.bisAntri.model.Notification;
import com.TKRPL.bisAntri.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private NotificationRepository notificationRepository;

    public SimpleMailMessage notifyUser(Notification notification, String sendTo){
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setFrom("noreply.bisantri@gmail.com");
        mail.setSubject("bisAntri Reminder Notification");
        mail.setText(notification.getPesanNotifikasi());
        mail.setTo(sendTo);

        javaMailSender.send(mail);
        notificationRepository.save(notification);
        return mail;
    }
}
