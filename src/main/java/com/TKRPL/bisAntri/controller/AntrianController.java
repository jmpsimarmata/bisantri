package com.TKRPL.bisAntri.controller;

import com.TKRPL.bisAntri.model.Antrian;
import com.TKRPL.bisAntri.service.AntrianService;
import com.TKRPL.bisAntri.service.GeneralUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/antrian")
public class AntrianController {

    @Autowired
    private AntrianService antrianService;

    @Autowired
    private GeneralUserServiceImpl generalUserServiceImpl;

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Antrian>> getAntrian() {
        return ResponseEntity.ok(antrianService.getAllAntrian());
    }

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Antrian> createAntrian(@RequestBody Antrian antrian){
        return ResponseEntity.ok(antrianService.createAntrian(antrian));
    }

    @PutMapping(path = "/{id}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Antrian> updateAntrian(@PathVariable(value = "id") int id,
                                                 @RequestBody Antrian antrian){
        return ResponseEntity.ok(antrianService.updateAntrian(id,antrian));
    }


    @PostMapping(path = "/antrianId={idAntrian}/userId={idUser}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Antrian> joinAntrian(@PathVariable(value = "idAntrian") int idAntrian, @PathVariable(value = "idUser") int idUser) {
        return ResponseEntity.ok(generalUserServiceImpl.joinAntrian(idAntrian, idUser));
    }


    @GetMapping(path = "/{id}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Antrian> getAntrianById(@PathVariable(value = "id") int id) {
        return ResponseEntity.ok(antrianService.getAntrianById(id));
    }

    @PostMapping(path = "/end/{id}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Antrian> finishAntrian(@PathVariable(value = "id")int id){
        return ResponseEntity.ok(antrianService.finishAntrian(id));

    }

    @PostMapping(path = "/next/{id}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Antrian> nextAntrian(@PathVariable(value = "id")int id){
        return ResponseEntity.ok(antrianService.nextAntrian(id));
    }

    @PostMapping(path = "/changeStatus/{id}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Antrian> changeStatusAntrian(@PathVariable(value = "id")int id){
        return ResponseEntity.ok(antrianService.changeStatusAntrian(id));
    }

}
