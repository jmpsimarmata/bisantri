package com.TKRPL.bisAntri.controller;

import com.TKRPL.bisAntri.model.GeneralUser;
import com.TKRPL.bisAntri.service.GeneralUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(originPatterns = "http://localhost:3000")
@RestController
@RequestMapping("/generaluser")
public class GeneralUserController {

    @Autowired
    private GeneralUserService generalUserService;

    @PostMapping(path="/register", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<GeneralUser> createGeneralUser(@RequestBody GeneralUser generalUser){
        return ResponseEntity.ok(generalUserService.createGeneralUser(generalUser));
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<GeneralUser>> getAllGeneralUsers() {
        return ResponseEntity.ok(generalUserService.getAllGeneralUsers());
    }

    @PostMapping(path = "/login/username={username}/password={password}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<GeneralUser> login(@PathVariable(value = "username") String username, @PathVariable(value = "password") String password) {
        GeneralUser user = generalUserService.loginWithGeneralUsernameAndPassword(username, password);
        if(user != null){
            return ResponseEntity.ok(user);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        return ResponseEntity.ok(generalUserService.loginWithGeneralUsernameAndPassword(username, password));
    }

    @GetMapping(path = "/userId={idUser}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<GeneralUser> getGeneralUserByIdUser(@PathVariable(value = "idUser") int idUser) {
        GeneralUser generalUser = generalUserService.getGeneralUserById(idUser);
        return ResponseEntity.ok(generalUser);
    }

    @GetMapping(path = "/username={username}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<GeneralUser> getGeneralUserByUsername(@PathVariable(value = "username") String username) {
        GeneralUser generalUser = generalUserService.getGeneralUserByUsername(username);
        return ResponseEntity.ok(generalUser);
    }

    @GetMapping(path = "/email={email}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<GeneralUser> getGeneralUserByEmail(@PathVariable(value = "email") String email) {
        GeneralUser generalUser = generalUserService.getGeneralUserByEmail(email);
        return ResponseEntity.ok(generalUser);
    }

}
