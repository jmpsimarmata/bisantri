package com.TKRPL.bisAntri.controller;

import com.TKRPL.bisAntri.model.History;
import com.TKRPL.bisAntri.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/history")
public class HistoryController {

    @Autowired
    private HistoryService historyService;

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<History>> getAllHistory() {
        return ResponseEntity.ok(historyService.getAllHistory());
    }

    @GetMapping(path = "/idGeneralUser={idGeneralUser}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<History>> getAllHistoryByIdGeneralUser(@PathVariable(value = "idGeneralUser") Integer idGeneralUser){
        return ResponseEntity.ok(historyService.getAllHistoryByIdGeneralUser(idGeneralUser));
    }

    @GetMapping(path = "idAntrian={idAntrian}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<History>> getAllHistoryAntrianByIdAntrian(@PathVariable(value = "idAntrian") Integer idAntrian){
        return ResponseEntity.ok(historyService.getAllHistoryByIdAntrian(idAntrian));
    }
}
