package com.TKRPL.bisAntri.model;

import com.TKRPL.bisAntri.model.Antrian;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "history")
@Setter
@Getter
public class History {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "uuid2")
    @Column(name = "id_history")
    private String idHistory;

    @Column(name = "role")
    private String role;

    @Column(name = "status")
    private Integer status;

    @Column(name = "`created_at`")
    @CreationTimestamp
    private Date createdTime;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedTime;

    @Column(name = "id_antrian")
    private Integer idAntrian;

    @Column(name = "nama_antrian")
    private String namaAntrian;

    @Column(name = "id_generalUser")
    private Integer idGeneralUser;

    @Column(name = "nomor_urut")
    private Integer nomorUrut;
}
