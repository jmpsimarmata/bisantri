package com.TKRPL.bisAntri.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "notification")
@Data
@NoArgsConstructor
@Getter
public class Notification {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "uuid2")
    @Column(name = "id_notifikasi")
    private String idNotifikasi;

    @Column(name = "pesan_notifikasi")
    private String pesanNotifikasi;

    public Notification(String pesanNotifikasi){
        this.pesanNotifikasi = pesanNotifikasi;
    }
}
