package com.TKRPL.bisAntri.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "antrian")
@Getter
@Setter
public class Antrian {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idAntrian;

    @Column(name = "nama", nullable = false )
    private String nama;

    @Column(name = "waktu_buka", nullable = false )
    private Date waktuBuka;

    @Column(name = "waktu_tutup", nullable = false )
    private Date waktuTutup;

    @Column(name = "kapasitas", nullable = false )
    private Integer kapasitas;

    @Column(name = "status", nullable = false )
    private Integer status;

    @Column(name = "id_checklist_requirements", nullable = false )
    private Integer idChecklistRequirements;

    @Column(name = "current_nomor_antrian", nullable = false )
    private Integer currentNomorAntrian;

    @Column(name = "jumlah_peserta", nullable = false )
    private Integer jumlahPeserta;

    @Column(name = "pemilik_antrian", nullable = false )
    private Integer pemilikAntrian;
}
