package com.TKRPL.bisAntri.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "general_user")
@Getter
@Setter
@NoArgsConstructor
public class GeneralUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idUser;

    @Column(name = "nama")
    private String nama;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "foto")
    private String foto;

    @Column(name = "current_joined_antrian_id")
    private Integer currentJoinedAntrianId;

    @Column(name = "current_managed_antrian_id")
    private Integer currentManagedAntrianId;

    public GeneralUser(String nama, String username, String password, String email, String foto, int currentJoinedAntrianId, int currentManagedAntrianId){
        this.nama = nama;
        this.username = username;
        this.password = password;
        this.email = email;
        this.foto = foto;
        this.currentJoinedAntrianId = currentJoinedAntrianId;
        this.currentManagedAntrianId = currentManagedAntrianId;
    }

}
