package com.TKRPL.bisAntri.repository;

import com.TKRPL.bisAntri.model.Antrian;
import com.TKRPL.bisAntri.model.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryRepository extends JpaRepository<History, String> {
    List<History> getHistoriesByIdAntrian(Integer idAntrian);
    List<History> getHistoriesByIdGeneralUserOrderByUpdatedTimeDesc(Integer idGeneralUser);
//    History getHistoryByIdAntrianAndiAndIdGeneralUser(String idAntrian, String idGeneralUser);
}
