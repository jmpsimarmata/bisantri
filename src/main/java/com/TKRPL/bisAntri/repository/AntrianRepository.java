package com.TKRPL.bisAntri.repository;

import com.TKRPL.bisAntri.model.Antrian;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface AntrianRepository extends JpaRepository <Antrian,String> {

    Antrian findByIdAntrian(Integer idAntrian);

}
