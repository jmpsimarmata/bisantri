package com.TKRPL.bisAntri.repository;

import com.TKRPL.bisAntri.model.GeneralUser;
import com.sun.tools.javac.jvm.Gen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GeneralUserRepository extends JpaRepository<GeneralUser, Integer> {
    GeneralUser getGeneralUserByIdUser(Integer idUser);
    GeneralUser getGeneralUserByEmail(String email);
    GeneralUser getGeneralUserByUsername(String username);
    GeneralUser getGeneralUserByUsernameAndPassword(String username, String password);
}
