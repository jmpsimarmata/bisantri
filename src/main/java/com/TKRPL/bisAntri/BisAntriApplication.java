package com.TKRPL.bisAntri;

import com.TKRPL.bisAntri.model.Notification;
import com.TKRPL.bisAntri.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class BisAntriApplication {

	public static void main(String[] args) {
		SpringApplication.run(BisAntriApplication.class, args);
	}

}


